import psutil,sys

def get_cpu(cpu_items):
	cpu_times = psutil.cpu_times()
	for item in cpu_items:
		if hasattr(cpu_times,item):
   		 prop = getattr(cpu_times,item)
   		 print(f"system.cpu.{item} : {prop}")

def get_mem(mem_items):
	mem_cap = psutil.virtual_memory()
	for item in mem_items:
		if hasattr(mem_cap,item):
   		 prop = getattr(mem_cap,item)
   		 print(f"virtual {item} : {prop}")
	smem_cap = psutil.swap_memory()
	for item in mem_items:
		if hasattr(smem_cap,item):
   		 prop = getattr(smem_cap,item)
   		 print(f"swap {item} : {prop}")   		     

if __name__ == "__main__":
 try:
  cpu  = ["idle","user","guest","iowait","steal","system","trurusdu"]
  mem  = ["total","used","free","shared"]
  if sys.argv[1] == "cpu":
   get_cpu(cpu)
  elif sys.argv[1] == "mem": 
   get_mem(mem)
  else:
   print("Usage ./script mem | ./script cpu") 
 except Exception as e:
  print(f"Exception:{e}, Usage ./script mem | ./script cpu")
