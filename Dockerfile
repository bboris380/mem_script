FROM python:3.8.7-slim-buster
RUN mkdir -p /home/script/
WORKDIR /home/script/
COPY ./ ./
RUN pip3 install -r requirements.txt
ENTRYPOINT ["/usr/local/bin/python3", "metrics.py"]
